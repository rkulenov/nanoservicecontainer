﻿using System;

namespace NanoServiceContainer.Service
{
    /// <summary>
    /// Definition of a service, provided by user.
    /// </summary>
    [Serializable]
    public struct ServiceMetadata : IEquatable<ServiceMetadata>
    {
        public string ServiceId;

        public string ServiceName;
        
        public ServiceMetadata(string serviceId, string serviceName)
        {
            ServiceId = serviceId;
            ServiceName = serviceName;
        }

        public override int GetHashCode()
        {
            return ServiceId.GetHashCode();
        }

        public bool Equals(ServiceMetadata other)
        {
            return String.Equals(ServiceId, other.ServiceId, StringComparison.InvariantCulture);
        }

        public override bool Equals(object obj)
        {
            return Equals((ServiceMetadata)obj);
        }

        public static bool operator ==(ServiceMetadata a, ServiceMetadata b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(ServiceMetadata a, ServiceMetadata b)
        {
            return !(a == b);
        }
    }
}
