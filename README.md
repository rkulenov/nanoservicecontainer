# README

**NanoServiceContainer** is a set of infrastructure components (libraries) for building composite applications consisting of multiple parts (services).
The container loads and starts services from subfolders using reflection. In microservices architecture each microservice is executed in LXC or VM, while in this project each nanoservice instance is ran in separate .Net application domain, but in single process.

More features:

* Hot services replacement - Nanoservices container allows replacing *.dlls of services and restarting services on the fly;
* REST API - you have an option to launch API to services container for accessing via HTTP;
* Health reports from services (currently in JSON format via REST API);

Solution contains 2 main projects:

* NanoServiceContainer - for service host implementation.
* NanoServiceContainer.Service - for classes and interfaces required by service instances.

See [wiki page](https://bitbucket.org/rkulenov/nanoservicecontainer/wiki/Home) for details.

* [UML diagrams](https://bitbucket.org/rkulenov/nanoservicecontainer/wiki/Home)
* [Service host implementation](https://bitbucket.org/rkulenov/nanoservicecontainer/wiki/Home)
* [Service implementation](https://bitbucket.org/rkulenov/nanoservicecontainer/wiki/Home)
* [Deployment](https://bitbucket.org/rkulenov/nanoservicecontainer/wiki/Home)

## NuGet

For creating host with services container:
> Install-Package NanoServiceContainer

For creating services:
> Install-Package NanoServiceContainer.Service

## REST API
** GET /services/metadata **

Gets metadata describing all services.

** GET /instances **

Gets all instances of services.

** POST /services/restart **

Restarts all services.

** POST /services/reload **

Reloads services metadata and starts all new services.

** POST /services/{serviceId}/restart **

Restarts services of specified type.

** POST /services/{serviceId}/uploadAsZip **

Uploads and extracts content of a zip file (from the request body) to the service's folder.
Then restarts the service. This method can be used to update service by uploading new dlls.
