﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using NanoServiceContainer.Service;
using WcfNanoCalculatorService;

namespace StubService
{
    [Service("wcf-calculator", "WCF calculator")]
    public class StubNanoService : ServiceInstanceBase
    {
        private ServiceHost _wcfHost;

        public StubNanoService()
        {

        }

        public override void Start()
        {
            State = InstanceState.Starting;
            try
            {
                _wcfHost = new ServiceHost(new NanoCalculatorService());
                _wcfHost.Open();
            }
            catch (AddressAccessDeniedException e)
            {
                // NOTE: It you get this exception use the following:
                // netsh http add urlacl url=http://+:15000/ user=your_user
                Console.WriteLine(e);
                throw;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            Console.WriteLine(" > WCF host created and listening at");
            Console.WriteLine("   " + _wcfHost.BaseAddresses.First());
            base.Start();
        }

        public override void Stop()
        {
            if (State != InstanceState.Started)
            {
                return;
            }
            State = InstanceState.Stopping;
            if (_wcfHost != null)
            {
                _wcfHost.Close();
            }
            Console.WriteLine("WCF host stopped");
            base.Stop();
        }

        public override string GetHealthStatusAsJson()
        {
            return "{'status': 'ok'}";
        }
    }
}