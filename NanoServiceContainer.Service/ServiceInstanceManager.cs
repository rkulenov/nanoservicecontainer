﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using NanoServiceContainer.Service.Logging;

namespace NanoServiceContainer.Service
{
    /// <summary>
    /// Infrastructure class.
    /// </summary>
    public sealed class ServiceInstanceManager : MarshalByRefObject, IServiceInstanceManager
    {
        /// <summary>
        /// Key is service id.
        /// </summary>
        private readonly Dictionary<string, ServiceDefinition> _definitions =
            new Dictionary<string, ServiceDefinition>();

        private readonly Dictionary<Guid, IServiceInstance> _instances = new Dictionary<Guid, IServiceInstance>();
        private ILogger _logger;

        public ServiceInstanceManager()
        {
            FindServices();
        }

        public static bool IsManagedAssembly(string fileName)
        {
            using (Stream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            using (BinaryReader binaryReader = new BinaryReader(fileStream))
            {
                if (fileStream.Length < 64)
                {
                    return false;
                }

                //PE Header starts @ 0x3C (60). Its a 4 byte header.
                fileStream.Position = 0x3C;
                uint peHeaderPointer = binaryReader.ReadUInt32();
                if (peHeaderPointer == 0)
                {
                    peHeaderPointer = 0x80;
                }

                // Ensure there is at least enough room for the following structures:
                //     24 byte PE Signature & Header
                //     28 byte Standard Fields         (24 bytes for PE32+)
                //     68 byte NT Fields               (88 bytes for PE32+)
                // >= 128 byte Data Dictionary Table
                if (peHeaderPointer > fileStream.Length - 256)
                {
                    return false;
                }

                // Check the PE signature.  Should equal 'PE\0\0'.
                fileStream.Position = peHeaderPointer;
                uint peHeaderSignature = binaryReader.ReadUInt32();
                if (peHeaderSignature != 0x00004550)
                {
                    return false;
                }

                // skip over the PEHeader fields
                fileStream.Position += 20;

                const ushort PE32 = 0x10b;
                const ushort PE32Plus = 0x20b;

                // Read PE magic number from Standard Fields to determine format.
                var peFormat = binaryReader.ReadUInt16();
                if (peFormat != PE32 && peFormat != PE32Plus)
                {
                    return false;
                }

                // Read the 15th Data Dictionary RVA field which contains the CLI header RVA.
                // When this is non-zero then the file contains CLI data otherwise not.
                ushort dataDictionaryStart = (ushort)(peHeaderPointer + (peFormat == PE32 ? 232 : 248));
                fileStream.Position = dataDictionaryStart;

                uint cliHeaderRva = binaryReader.ReadUInt32();
                if (cliHeaderRva == 0)
                {
                    return false;
                }

                return true;
            }
        }

        private void FindServices()
        {
            _definitions.Clear();

            foreach (var fileName in Directory.EnumerateFiles(AppDomain.CurrentDomain.BaseDirectory))
            {
                if ((fileName.EndsWith(".exe") || fileName.EndsWith(".dll"))
                    && IsManagedAssembly(fileName))
                {
                    foreach (var def in FindServiceType(fileName))
                    {
                        _definitions.Add(def.Metadata.ServiceId, def);
                    }
                }
            }
        }

        private IEnumerable<ServiceDefinition> FindServiceType(string assemblyPath)
        {
            var res = new List<ServiceDefinition>();
            try
            {
                var a = Assembly.LoadFrom(assemblyPath);
                foreach (var assemblyName in a.GetReferencedAssemblies())
                {
                    try
                    {
                        Assembly.ReflectionOnlyLoad(assemblyName.FullName);
                    }
                    catch
                    {
                        try
                        {
                            Assembly.LoadFrom(Path.Combine(Path.GetDirectoryName(assemblyPath),
                                assemblyName.Name + ".dll"));
                        }
                        catch (Exception e)
                        {
                            //_logger.Log("Failed to preload referenced assembly " + assemblyName.Name + ".dll" + e.Message);
                        }
                    }
                }
                foreach (var t in a.ExportedTypes)
                {
                    foreach (var ca in t.CustomAttributes)
                    {
                        if (ca.AttributeType.FullName.Equals(typeof(ServiceAttribute).FullName))
                        {
                            var sa = t.GetCustomAttribute<ServiceAttribute>();
                            var m = new ServiceMetadata(sa.Id, sa.Name);
                            res.Add(new ServiceDefinition(m, t));
                        }
                    }
                }
            }
            catch (BadImageFormatException)
            {
                _logger.Log("BadImageFormat: {0}", assemblyPath);
            }
            return res;
        }

        IList<ServiceMetadata> IServiceInstanceManager.ServiceMetadatas
        {
            get { return _definitions.Values.Select(d => d.Metadata).ToList(); }
        }


        IServiceInstance IServiceInstanceManager.CreateInstance(string serviceId)
        {
            _logger.Log("Creating instance of service id={0} ...", serviceId);
            var t = _definitions[serviceId].ServiceType;
            var inst = (IServiceInstance)Activator.CreateInstance(t);
            _instances.Add(inst.Id, inst);
            return inst;
        }


        IList<IServiceInstance> IServiceInstanceManager.Instances
        {
            get { return _instances.Values.ToList(); }
        }


        void IServiceInstanceManager.InstantiateAllServices()
        {
            foreach (var serviceDefinition in _definitions)
            {
                try
                {
                    ((IServiceInstanceManager) this).CreateInstance(serviceDefinition.Key);
                }
                catch (Exception e)
                {
                    _logger.Log(e);
                }
            }
        }

        void IServiceInstanceManager.DestroyAllServices()
        {
            foreach (var serviceInstance in _instances)
            {
                try
                {
                    _logger.Log("Disposing instance of service id={0} ...", serviceInstance.Value.Id);
                    serviceInstance.Value.Dispose();
                }
                catch (Exception e)
                {
                    _logger.Log(e);
                }
            }
            _logger.Log("Destroyed all instances.");
            _instances.Clear();
        }


        void IServiceInstanceManager.StartAllInstances()
        {
            foreach (var serviceInstance in _instances)
            {
                try
                {
                    _logger.Log("Starting service instance id={0} ...", serviceInstance.Value.Id);
                    serviceInstance.Value.Start();
                }
                catch (Exception e)
                {
                    _logger.Log(e);
                }
            }
        }

        void IServiceInstanceManager.StopAllInstances()
        {
            foreach (var serviceInstance in _instances)
            {
                try
                {
                    _logger.Log("Stopping instance of service id={0} ...", serviceInstance.Value.Id);
                    serviceInstance.Value.Stop();
                }
                catch (Exception e)
                {
                    _logger.Log(e);
                }
            }
        }

        public void SetLogger(Logging.ILogger logger)
        {
            _logger = logger;
        }
    }
}
