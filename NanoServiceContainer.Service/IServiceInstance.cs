﻿using System;

namespace NanoServiceContainer.Service
{
    /// <summary>
    /// Interface of the service instance.
    /// </summary>
    public interface IServiceInstance : IDisposable
    {
        /// <summary>
        /// Gets ID of the instance.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Gets state of the service instance.
        /// </summary>
        InstanceState State { get; }

        /// <summary>
        /// Starts the service instance.
        /// </summary>
        void Start();

        /// <summary>
        /// Stops the service instance.
        /// </summary>
        void Stop();

        /// <summary>
        /// Gets serialized service health status.
        /// Implementation shall be thread-safe.
        /// </summary>
        /// <returns></returns>
        string GetHealthStatusAsJson();

        /// <summary>
        /// Gets service metadata.
        /// </summary>
        /// <returns></returns>
        ServiceMetadata GetMetadata();
    }
}
