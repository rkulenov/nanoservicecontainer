﻿using System;

namespace NanoServiceContainer.Service
{
    internal sealed class ServiceDefinition
    {
        public ServiceMetadata Metadata { get; private set; }
        public string AssemblyName { get; private set; }
        public string ClassName { get; private set; }
        public Type ServiceType { get; private set; }

        public ServiceDefinition(ServiceMetadata metadata, Type serviceType)
        {
            Metadata = metadata;
            ServiceType = serviceType;
            AssemblyName = serviceType.Assembly.FullName;
            ClassName = serviceType.Name;
        }
    }
}
