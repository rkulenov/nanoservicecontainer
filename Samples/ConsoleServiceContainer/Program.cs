﻿using System;
using log4net;
using log4net.Config;
using NanoServiceContainer;
using NanoServiceContainer.Api;
using NanoServiceContainer.Service.Logging;

namespace ConsoleServiceContainer
{
    class Program : MarshalByRefObject, ILogger
    {
        private ILog _logger = LogManager.GetLogger("host");

        static void Main(string[] args)
        {
            XmlConfigurator.Configure();

            var logger = new Program();
            Console.WriteLine("Starting host...");
            Container c = new Container(logger);
            WebApiHost host = new WebApiHost(c, new Uri(@"http://localhost:8000"), logger);

            Console.WriteLine("Host started. Management URL: http://localhost:8000");
            Console.WriteLine("Press ENTER to exit");
            Console.ReadLine();
            Console.WriteLine("Disposing...");
            host.Dispose();
            c.Dispose();
        }

        public void Log(LogEntry entry)
        {
            switch (entry.Severity)
            {
                case LoggingEventType.Debug:
                    _logger.Debug(entry.Message);
                    break;
                case LoggingEventType.Information:
                    _logger.Info(entry.Message);
                    break;
                case LoggingEventType.Warning:
                    _logger.Warn(entry.Message);
                    break;
                case LoggingEventType.Error:
                    _logger.Error(entry.Message, entry.Exception);
                    break;
                case LoggingEventType.Fatal:
                    _logger.Error(entry.Message, entry.Exception);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override object InitializeLifetimeService()
        {
            // Remote proxy to this object will live forever.
            return null;
        }

    }
}
