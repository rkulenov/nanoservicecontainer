﻿using System;
using System.ServiceModel;

namespace WcfNanoCalculatorService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class NanoCalculatorService : INanoCalculatorService
    {

        double INanoCalculatorService.Sum(CompositeType v)
        {
            return v.FirstOperand + v.SecondOperand;
        }
    }
}