﻿namespace NanoServiceContainer.Service
{
    /// <summary>
    /// State of service instances.
    /// </summary>
    public enum InstanceState : byte
    {
        Undefined,
        Starting,
        Started,
        Stopping,
        Stopped,
        Disposed,
        Failed
    }
}