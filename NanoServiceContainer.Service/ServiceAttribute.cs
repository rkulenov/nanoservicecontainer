﻿using System;

namespace NanoServiceContainer.Service
{
    /// <summary>
    /// Mark your service with this attribute.
    /// </summary>
    public class ServiceAttribute : Attribute
    {
        /// <summary>
        /// ID of the service.
        /// </summary>
        public string Id { get; set; }
        public string Name { get; set; }

        public ServiceAttribute(string id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
