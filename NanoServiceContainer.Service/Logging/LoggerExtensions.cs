﻿using System;

namespace NanoServiceContainer.Service.Logging
{
    public static class LoggerExtensions
    {
        public static void Log(this ILogger logger, string format, params object[] args)
        {
            if (logger == null)
            {
                return;
            }
            logger.Log(new LogEntry(LoggingEventType.Debug, String.Format(format, args)));
        }

        public static void Log(this ILogger logger, string message)
        {
            if (logger == null)
            {
                return;
            }
            logger.Log(new LogEntry(LoggingEventType.Information, message));
        }

        public static void Log(this ILogger logger, Exception exception)
        {
            if (logger == null)
            {
                return;
            }
            logger.Log(new LogEntry(LoggingEventType.Error, exception.Message, exception));
        }
    }
}
