﻿using System;
using System.Dynamic;
using NanoServiceContainer.Service;

namespace NanoServiceContainer.Api
{
    public class InstanceDescr
    {
        public Guid InstanceId;
        public string ServiceName;
        public string ServiceId;
        public InstanceState State;
        public string Health;
        public string AppDomain;
    }
}
