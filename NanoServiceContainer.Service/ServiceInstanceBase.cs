﻿using System;
using System.Reflection;

namespace NanoServiceContainer.Service
{
    /// <summary>
    /// Base class for creating nano services.
    /// </summary>
    public abstract class ServiceInstanceBase : MarshalByRefObject, IServiceInstance
    {
        protected bool _isDisposed;

        /// <summary>
        /// Gets the instance state.
        /// </summary>
        public InstanceState State { get; protected set; }

        /// <summary>
        /// Gets ID of the service.
        /// </summary>
        public Guid Id { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceInstanceBase"/> class.
        /// </summary>
        protected ServiceInstanceBase()
        {
            Id = Guid.NewGuid();
            State = InstanceState.Stopped;
        }

        /// <summary>
        /// Starts new instance of the service.
        /// </summary>
        public virtual void Start()
        {
            State = InstanceState.Started;
        }

        /// <summary>
        /// Stops the service instance.
        /// </summary>
        public virtual void Stop()
        {
            State = InstanceState.Stopped;
        }

        public void Dispose()
        {
            if (_isDisposed)
            {
                return;
            }
            Stop();
            State = InstanceState.Disposed;
            _isDisposed = true;
            Dispose(true);
        }

        protected virtual void Dispose(bool isDisposing)
        {

        }
        
        public virtual string GetHealthStatusAsJson()
        {
            return null;
        }

        public ServiceMetadata GetMetadata()
        {
            var a = (ServiceAttribute)GetType().GetCustomAttribute(typeof (ServiceAttribute));
            return new ServiceMetadata(a.Id, a.Name);
        }
    }
}
