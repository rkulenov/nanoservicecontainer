﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using NanoServiceContainer.Service;
using System.IO;

namespace NanoServiceContainer.Api
{
    /// <summary>
    /// REST API for service container.
    /// </summary>
    [ServiceContract]
    interface IServiceContainer
    {
        /// <summary>
        /// Gets metadata describing all services.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "services/metadata")]
        IEnumerable<ServiceMetadata> GetServicesMetadata();

        /// <summary>
        /// Gets all instances of services.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "instances")]
        IEnumerable<InstanceDescr> GetInstances();

        /// <summary>
        /// Restarts all services.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "services/restart")]
        bool RestartAll();

        /// <summary>
        /// Loads and starts not existing services.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "services/reload")]
        bool Reload();

        /// <summary>
        /// Restarts services of specified type.
        /// </summary>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "services/{serviceId}/restart")]
        bool RestartInstancesOfService(string serviceId);

        /// <summary>
        /// Uploads service dlls zipped in archive and restarts service.
        /// </summary>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "services/{serviceId}/uploadAsZip")]
        bool UploadAsZip(string serviceId, Stream fileContents);
    }
}
