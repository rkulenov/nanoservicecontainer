﻿using System;
using System.Runtime.Remoting.Lifetime;
using NanoServiceContainer.Service;

namespace NanoServiceContainer
{
    /// <summary>
    /// Describes folder of service (manager of all services from that folder, appDomain services were loaded into).
    /// This class also tracks lifetime of references to IServiceInstanceManager.
    /// </summary>
    public class ServiceDomainItem : MarshalByRefObject, IEquatable<ServiceDomainItem>, ISponsor, IDisposable
    {
        private readonly IServiceInstanceManager _manager;
        private readonly AppDomain _domain;
        private readonly string _servicePath;
        private bool _isDisposed;

        public ServiceDomainItem(IServiceInstanceManager m, AppDomain ad, string servicePath)
        {
            _domain = ad;
            _servicePath = servicePath;
            _manager = m;

            var lease = ((MarshalByRefObject)m).InitializeLifetimeService() as ILease;
            if (lease != null)
            {
                lease.Register(this);
            }
        }

        ~ServiceDomainItem()
        {
            Dispose();
        }

        public bool IsDisposed
        {
            get { return _isDisposed; }
        }

        public IServiceInstanceManager Manager
        {
            get { return _manager; }
        }

        public AppDomain Domain
        {
            get { return _domain; }
        }

        public string ServicePath
        {
            get { return _servicePath; }
        }

        public override int GetHashCode()
        {
            return _servicePath.GetHashCode();
        }

        public bool Equals(ServiceDomainItem other)
        {
            return ServicePath.Equals(other.ServicePath);
        }

        public override bool Equals(object obj)
        {
            return Equals((ServiceDomainItem)obj);
        }

        TimeSpan ISponsor.Renewal(ILease lease)
        {
            if (lease == null || _isDisposed)
            {
                return TimeSpan.Zero;
            }
            return TimeSpan.FromSeconds(10);
        }

        public override object InitializeLifetimeService()
        {
            // Remote proxy to this object will live forever.
            return null;
        }

        public void Dispose()
        {
            _isDisposed = true;
        }
    }
}
