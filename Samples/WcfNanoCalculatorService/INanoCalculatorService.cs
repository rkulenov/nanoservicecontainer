﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace WcfNanoCalculatorService
{
    [ServiceContract]
    public interface INanoCalculatorService
    {
        [OperationContract]
        double Sum(CompositeType composite);
    }

    [DataContract]
    public class CompositeType
    {
        private double _firstOperand;
        private double _secondOperand;

        [DataMember]
        public double FirstOperand
        {
            get { return _firstOperand; }
            set { _firstOperand = value; }
        }

        [DataMember]
        public double SecondOperand
        {
            get { return _secondOperand; }
            set { _secondOperand = value; }
        }
    }
}