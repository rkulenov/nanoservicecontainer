﻿using System;
using System.Reflection;
using NanoServiceContainer.Service;
using Newtonsoft.Json;

namespace StubService
{
    [Service("number-one-service-id", "Service number one")]
    public class StubNanoService : ServiceInstanceBase
    {
        public StubNanoService()
        {

        }

        public override void Start()
        {
            State = InstanceState.Starting;
            Console.WriteLine(" > Yeaaahh! Stub service started");
            base.Start();
        }

        public override void Stop()
        {
            if (State != InstanceState.Started)
            {
                return;
            }
            State = InstanceState.Stopping;
            Console.WriteLine("Stub service stopped");
            base.Stop();
        }

        public override string GetHealthStatusAsJson()
        {
            var m = GetMetadata();
            var res = new
            {
                id = m.ServiceId,
                name = m.ServiceName,
                state = this.State,
                domain = AppDomain.CurrentDomain.FriendlyName
            };
            return JsonConvert.SerializeObject(res);
        }
    }
}
