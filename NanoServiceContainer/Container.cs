﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using NanoServiceContainer.Service;
using NanoServiceContainer.Service.Logging;

namespace NanoServiceContainer
{
    /// <summary>
    /// Container of nano services.
    /// </summary>
    public class Container : MarshalByRefObject, IDisposable
    {
        private const string SERVICE_DLL = @"\NanoServiceContainer.Service.dll";
        private readonly ILogger _logger;
        private volatile bool _isDisposed;
        private volatile bool _isRestarting;

        /// <summary>
        /// Key is subfolder name.
        /// </summary>
        private readonly ConcurrentDictionary<string, ServiceDomainItem> _domains =
            new ConcurrentDictionary<string, ServiceDomainItem>();

        /// <summary>
        /// Information about created appDomains and loaded services.
        /// </summary>
        public IReadOnlyList<ServiceDomainItem> Domains
        {
            get { return _domains.Values.Where(d=>!d.IsDisposed).ToList(); }
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="Container"/> class.
        /// </summary>
        /// <param name="logger">Logger, or null.</param>
        public Container(ILogger logger = null)
        {
            _logger = logger;
            Start();
        }

        /// <summary>
        /// Restarts all services.
        /// </summary>
        /// <param name="serviceId">If provided, then only this service is restarted.</param>
        /// <returns></returns>
        public bool Restart(string serviceId = null)
        {
            if (_isRestarting)
            {
                return false;
            }

            if (serviceId == null)
            {
                _logger.Log("Restarting all services...");
            }
            else
            {
                _logger.Log("Restarting service id={0}", serviceId);
            }
            try
            {
                var tasks = new List<Task>();

                foreach (var item in _domains)
                {
                    if (serviceId == null ||
                        item.Value.Manager.ServiceMetadatas.Contains(new ServiceMetadata(serviceId, "")))
                    {
                        var t = Task.Factory.StartNew((state) =>
                        {
                            var sdi = (ServiceDomainItem)state;
                            try
                            {
                                sdi.Manager.DestroyAllServices();
                            }
                            catch (Exception e)
                            {
                                _logger.Log(e);
                            }
                            try
                            {
                                _logger.Log("Unloading domain {0} ", sdi.Domain.FriendlyName);
                                AppDomain.Unload(sdi.Domain);
                                sdi.Dispose();
                            }
                            catch (Exception e)
                            {
                                _logger.Log(e);
                            }
                            try
                            {
                                LoadServiceFromFolder(sdi.ServicePath);
                            }
                            catch (Exception e)
                            {
                                _logger.Log(e);
                            }
                        }, item.Value);
                        tasks.Add(t);
                    }
                }

                Task.WaitAll(tasks.ToArray());
                _logger.Log("All services reloaded");
                return true;
            }
            finally
            {
                _isRestarting = false;
            }
        }

        /// <summary>
        /// Loads not existing services.
        /// </summary>
        public void  Reload()
        {
            var tasks = new List<Task>();
            var dirs = Directory.EnumerateDirectories(AppDomain.CurrentDomain.BaseDirectory);
            foreach (var dir in dirs)
            {
                if (!_domains.ContainsKey(dir))
                {
                    var t = Task.Factory.StartNew((state) => LoadServiceFromFolder((string) state), dir);
                    tasks.Add(t);
                }
            }
            Task.WaitAll(tasks.ToArray());
        }

        /// <summary>
        /// Starts service container, loads all services from subfolders and starts them.
        /// </summary>
        public void Start()
        {
            _logger.Log("Starting service container...");
            var tasks = new List<Task>();
            var dirs = Directory.EnumerateDirectories(AppDomain.CurrentDomain.BaseDirectory);
            foreach (var dir in dirs)
            {
                var t = Task.Factory.StartNew((state) => LoadServiceFromFolder((string) state), dir);
                tasks.Add(t);
            }
            Task.WaitAll(tasks.ToArray());
            _logger.Log("Service container started.");
        }

        /// <summary>
        /// Stops all services and disposes all appDomains.
        /// </summary>
        public void Stop()
        {
            _logger.Log("Stopping and disposing all services...");
            var tasks = new List<Task>();
            foreach (var item in _domains)
            {
                var t = Task.Factory.StartNew((state) =>
                {
                    var sdi = (ServiceDomainItem) state;
                    try
                    {
                        sdi.Manager.DestroyAllServices();
                    }
                    catch (Exception e)
                    {
                        _logger.Log(e);
                    }
                    try
                    {
                        _logger.Log("Unloading domain {0} ", sdi.Domain.FriendlyName);
                        AppDomain.Unload(sdi.Domain);
                        sdi.Dispose();
                    }
                    catch (Exception e)
                    {
                        _logger.Log(e);
                    }
                }, item.Value);
                tasks.Add(t);
            }
            Task.WaitAll(tasks.ToArray());
            _domains.Clear();
            _logger.Log("All services disposed");
        }

        private void LoadServiceFromFolder(string servicePath)
        {
            if (!File.Exists(servicePath + SERVICE_DLL))
            {
                return;
            }
            _logger.Log("Loading service from " + servicePath);

            try
            {
                var ads = new AppDomainSetup()
                {
                    ShadowCopyFiles = "true",
                    //ads.SetConfigurationBytes();
                    ApplicationBase = servicePath,
                    PrivateBinPath = ".",
                    ConfigurationFile = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile
                };

                var ad = AppDomain.CreateDomain("service " + Path.GetFileName(servicePath), AppDomain.CurrentDomain.Evidence, ads);

                try
                {
                    var instanceManager =
                        (IServiceInstanceManager) ad.CreateInstanceAndUnwrap(typeof (ServiceInstanceManager).Assembly.FullName,
                            typeof (ServiceInstanceManager).FullName, null);
                    instanceManager.SetLogger(_logger);

                    foreach (var s in instanceManager.ServiceMetadatas)
                    {
                        _logger.Log("Service found: {0} ({1})", s.ServiceName, s.ServiceId);
                    }

                    instanceManager.InstantiateAllServices();
                    instanceManager.StartAllInstances();

                    _domains.AddOrUpdate(servicePath,
                        s => new ServiceDomainItem(instanceManager, ad, servicePath),
                        (s, d) =>
                        {
                            d.Dispose();
                            return new ServiceDomainItem(instanceManager, ad, servicePath);
                        });
                }
                catch (Exception e)
                {
                    _logger.Log(e);
                    _logger.Log("Unloading appDomain ...");
                    AppDomain.Unload(ad);
                }
            }
            catch (Exception e)
            {
                _logger.Log(e);
            }
        }

        public void Dispose()
        {
            _logger.Log("Disposing...");
            if (_isDisposed)
            {
                return;
            }
            _isDisposed = true;
            Stop();
        }
    }
}