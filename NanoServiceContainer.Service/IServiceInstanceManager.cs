﻿using System.Collections.Generic;
using NanoServiceContainer.Service.Logging;

namespace NanoServiceContainer.Service
{
    public interface IServiceInstanceManager
    {
        void SetLogger(ILogger logger);
        IList<ServiceMetadata> ServiceMetadatas { get; }
        IServiceInstance CreateInstance(string serviceId);
        void InstantiateAllServices();
        void DestroyAllServices();
        void StartAllInstances();
        void StopAllInstances();
        IList<IServiceInstance> Instances { get; }
    }
}
