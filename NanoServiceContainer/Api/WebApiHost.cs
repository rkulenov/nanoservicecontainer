﻿using System;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using NanoServiceContainer.Service.Logging;

namespace NanoServiceContainer.Api
{
    /// <summary>
    /// Host supporting REST API for provided service container.
    /// </summary>
    public class WebApiHost : IDisposable
    {
        private readonly ILogger _logger;
        private ContainerWebApi _container;
        private WebServiceHost _host;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebApiHost"/> class.
        /// </summary>
        /// <param name="container">Service container</param>
        /// <param name="uri">URI for REST API</param>
        /// <param name="logger">Logger, or null.</param>
        public WebApiHost(Container container, Uri uri, ILogger logger = null)
        {
            _logger = logger;
            _container = new ContainerWebApi(container, logger);

            _host = new WebServiceHost(_container, uri);
            var b = new WebHttpBinding()
            {
                MaxBufferPoolSize = 2147483647,
                MaxBufferSize = 2147483647,
                MaxReceivedMessageSize = 2147483647
            };
            ServiceEndpoint ep = _host.AddServiceEndpoint(typeof(IServiceContainer), b, "");
            ServiceDebugBehavior stp = _host.Description.Behaviors.Find<ServiceDebugBehavior>();
            stp.HttpHelpPageEnabled = false;
            _host.Open();
            _logger.Log("WEB API is up and running at {0}", _host.BaseAddresses.First());
        }

        public void Dispose()
        {
            _host.Close();
        }
    }
}
