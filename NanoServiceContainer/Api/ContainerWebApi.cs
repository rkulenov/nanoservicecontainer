﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using NanoServiceContainer.Service;
using NanoServiceContainer.Service.Logging;
using System.IO.Compression;
using System.Linq;

namespace NanoServiceContainer.Api
{
    /// <summary>
    /// REST API for service container.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    internal class ContainerWebApi: IServiceContainer
    {
        private readonly Container _container;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContainerWebApi"/> class.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="logger"></param>
        public ContainerWebApi(Container container, ILogger logger = null)
        {
            _container = container;
            _logger = logger;
        }

        IEnumerable<Service.ServiceMetadata> IServiceContainer.GetServicesMetadata()
        {
            _logger.Log(@"GET /services/metadata");

            try
            {
                var res = new List<ServiceMetadata>();
                foreach (var d in _container.Domains)
                {
                    res.AddRange(d.Manager.ServiceMetadatas);
                }
                return res;
            }
            catch (Exception e)
            {
                _logger.Log(e);
                throw;
            }
        }

        IEnumerable<InstanceDescr> IServiceContainer.GetInstances()
        {
            _logger.Log(@"GET /instances");

            try
            {
                var res = new List<InstanceDescr>();
                foreach (var d in _container.Domains)
                {
                    foreach (var inst in d.Manager.Instances)
                    {
                        res.Add(new InstanceDescr()
                        {
                            InstanceId = inst.Id,
                            State = inst.State,
                            Health = inst.GetHealthStatusAsJson(),
                            ServiceId = inst.GetMetadata().ServiceId,
                            ServiceName = inst.GetMetadata().ServiceName,
                            AppDomain = d.Domain.FriendlyName
                        });
                    }
                }
                return res;
            }
            catch (Exception e)
            {
                _logger.Log(e);
                throw;
            }
        }

        bool IServiceContainer.RestartAll()
        {
            _logger.Log(@"POST /services/restart");
            try
            {
                return _container.Restart();
            }
            catch (Exception e)
            {
                _logger.Log(e);
                throw;
            }
        }

        bool IServiceContainer.Reload()
        {
            _logger.Log(@"POST /services/reload");
            try
            {
                _container.Reload();
                return true;
            }
            catch (Exception e)
            {
                _logger.Log(e);
                throw;
            }
        }

        bool IServiceContainer.RestartInstancesOfService(string serviceId)
        {
            _logger.Log(@"POST /services/{0}/restart", serviceId);
            try
            {
                return _container.Restart(serviceId);
            }
            catch (Exception e)
            {
                _logger.Log(e);
                throw;
            }
        }

        bool IServiceContainer.UploadAsZip(string serviceId, System.IO.Stream fileContents)
        {
            _logger.Log(@"POST /services/{0}/uploadAsZip", serviceId);
            try
            {
                using (var zip = new ZipArchive(fileContents, ZipArchiveMode.Read))
                {
                    string serviceFolder = null;
                    foreach (var sdi in _container.Domains)
                    {
                        if (sdi.Manager.ServiceMetadatas.FirstOrDefault(sd => sd.ServiceId == serviceId) != null)
                        {
                            serviceFolder = sdi.ServicePath;
                            break;
                        }
                    }

                    if (serviceFolder == null)
                    {
                        _logger.Log("Service not found {0}", serviceId);
                        return false;
                    }

                    _logger.Log("Extracting to {0}", serviceFolder);
                    zip.ExtractToDirectory(serviceFolder);
                }
                
                return _container.Restart(serviceId);
            }
            catch (Exception e)
            {
                _logger.Log(e);
                throw;
            }
        }

    }
}
